package br.edu.uepb.nutes.simplesurvey.example;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


import br.edu.uepb.nutes.simplesurvey.base.SimpleSurvey;
import br.edu.uepb.nutes.simplesurvey.question.Dichotomic;
import br.edu.uepb.nutes.simplesurvey.question.Infor;
import br.edu.uepb.nutes.simplesurvey.question.Multiple;
import br.edu.uepb.nutes.simplesurvey.question.Open;
import br.edu.uepb.nutes.simplesurvey.question.Single;
public class SimpleSurvey1 extends SimpleSurvey implements Infor.OnInfoListener,
        Dichotomic.OnDichotomicListener, Single.OnSingleListener,
        Multiple.OnMultipleListener,
        Open.OnTextBoxListener {
    private final String LOG_TAG = SimpleSurvey1.class.getSimpleName();
    private static final String FILE_NAME = "answers.txt";
    private List<String> list = new ArrayList<String>();
    @Override
    protected void initView() {
        addPages();
    }

    private void addPages() {
        setMessageBlocked("Oops! answer so i can go to the next question...");
        /**
         * Available animations:
         *    - setFadeAnimation()
         *    - setZoomAnimation()
         *    - setFlowAnimation()
         *    - setSlideOverAnimation()
         *    - setDepthAnimation()
         * More details: {https://github.com/AppIntro/AppIntro#animations}
         */
        setFadeAnimation();
        int pageNum = 0;
        addQuestion(new Infor.Config()
                .layout(R.layout.welcome)
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());



        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("How old are you?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorCyan))
                .image(R.drawable.age)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("Below 18");
                    add("18");
                    add("19");
                    add("20");
                    add("Above 20");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Dichotomic.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("What is your gender?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorTeal))
                .buttonClose(R.drawable.ic_action_close_dark)
                .image(R.drawable.gender)
                //.enableZoomImage()
                .inputStyle(R.drawable.radio_sample1_lef, R.drawable.radio_sample1_right,
                        Color.WHITE, Color.WHITE)
                .inputLeftText(R.string.masc)
                .inputRightText(R.string.femi)
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("Are you Hispanic or Latino?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorOrange))
                .image(R.drawable.hispanic)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("Yes");
                    add("No");
                    add("Don't know / not sure");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("Which one or more of the following would you say is your race?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorIndigo))
                .image(R.drawable.race)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("White");
                    add("Black or African American");
                    add("Asian");
                    add("Native Hawaiian or other Pacific Islander");
                    add("American Indian or Alaska Native");
                    add("Don't know / not sure");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Dichotomic.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("Are you a US or international student?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorIndigo))
                .buttonClose(R.drawable.ic_action_close_dark)
                .image(R.drawable.international)
                //.enableZoomImage()
                .inputStyle(R.drawable.radio_sample1_lef, R.drawable.radio_sample1_right,
                        Color.WHITE, Color.WHITE)
                .inputLeftText(R.string.us)
                .inputRightText(R.string.international)
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Dichotomic.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("Are you planning to be involved in ROTC?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorCyan))
                .buttonClose(R.drawable.ic_action_close_dark)
                .image(R.drawable.rotc)
                //.enableZoomImage()
                .inputStyle(R.drawable.radio_sample1_lef, R.drawable.radio_sample1_right,
                        Color.WHITE, Color.WHITE)
                .inputLeftText(R.string.yes)
                .inputRightText(R.string.no)
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Dichotomic.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("Are you in Trinity or Pratt?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorBlueGrey))
                .buttonClose(R.drawable.ic_action_close_dark)
                .image(R.drawable.duke)
                //.enableZoomImage()
                .inputStyle(R.drawable.radio_sample1_lef, R.drawable.radio_sample1_right,
                        Color.WHITE, Color.WHITE)
                .inputLeftText(R.string.trinity)
                .inputRightText(R.string.pratt)
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Open.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("What is your intended major/minor? (Format: major / minor)",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorDeepPurple))
                .image(R.drawable.major)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(ContextCompat.getColor(this, R.color.colorAccent))
                .inputColorText(Color.WHITE)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("How many classes are you enrolled in for the Fall Semester?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorTeal))
                .image(R.drawable.course)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("Less than 4 classes");
                    add("4 classes");
                    add("4.5 classes");
                    add("5 classes");
                    add("More than 5 classes");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Open.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("Please list the classes you are currently enrolled in for Fall 2019 (e.g., BIO325):",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorDeepPurple))
                .image(R.drawable.class1)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(ContextCompat.getColor(this, R.color.colorAccent))
                .inputColorText(Color.WHITE)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("What time is your earliest scheduled class Monday-Friday?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorAmber))
                .image(R.drawable.earliest)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("8:30");
                    add("10:05");
                    add("11:45");
                    add("1:25");
                    add("3:05 or later");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Dichotomic.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("Are you on a varsity sports team?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorIndigo))
                .buttonClose(R.drawable.ic_action_close_dark)
                .image(R.drawable.sportsteam)
                //.enableZoomImage()
                .inputStyle(R.drawable.radio_sample1_lef, R.drawable.radio_sample1_right,
                        Color.WHITE, Color.WHITE)
                .inputLeftText(R.string.yes)
                .inputRightText(R.string.no)
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("What floor are you assigned to?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorTeal))
                .image(R.drawable.floor)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("1");
                    add("2");
                    add("3");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("How many roommates do you have?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorBrown))
                .image(R.drawable.dormitory)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("0");
                    add("1");
                    add("2");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());


        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("What kind of cell phone do you own?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorLightBlue))
                .image(R.drawable.phone)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("iOS (iPhone)");
                    add("Android (e.g., Samsung)");
                    add("Windows");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());

        addQuestion(new Single.Config()
                .title("Initial Demographic Questions", Color.WHITE)
                .description("What kind of monthly data plan do you have?",
                        Color.WHITE)
                .descriptionTextSize(24)
                .colorBackground(ContextCompat.getColor(this, R.color.colorLightGreen))
                .image(R.drawable.dataplan)
                .buttonClose(R.drawable.ic_action_close_dark)
                .inputColorBackgroundTint(Color.WHITE)
                .inputColorSelectedText(Color.WHITE)
                .inputItems(new ArrayList<String>() {{
                    add("Unlimited data");
                    add("Limited data (below 1 G)");
                    add("Limited data (below 5 G)");
                    add("Limited data (below 10 G)");
                    add("Limited data (above 10 G)");
                }})
                .inputDisableAddNewItem()
                .nextQuestionAuto()
                .pageNumber(pageNum++)
                .build());


        addQuestion(new Infor.Config()
                .layout(R.layout.save_and_load)
                .build());

        addQuestion(new Infor.Config()
                .title("Thank you for the answers :)")
                .pageNumber(-1)
                .build());




    }

    public void save_answers(View view) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i));
        }
        String text = sb.toString();
        FileOutputStream fos = null;

        try {
            fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
            fos.write(text.getBytes());
            Toast.makeText(this, "Saved to " + getFilesDir() + "/" + FILE_NAME,
                    Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClosePage() {
        new AlertDialog
                .Builder(this)
                .setMessage("Do you want to cancel the survey??")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void onAnswerInfo(int page) {
        Log.d(LOG_TAG, "onAnswerInfo() | PAGE: " + page);
        if (page == -1) { // end page
            finish();
        }
    }

    @Override
    public void onAnswerDichotomic(int page, boolean value) {
        Log.d(LOG_TAG, "onAnswerDichotomic() | PAGE:  " + page + " | ANSWER: " + value);
    }

    @Override
    public void onAnswerSingle(int page, String value, int indexValue) {
        Log.d(LOG_TAG, "onAnswerMultiple() | PAGE:  " + page
                + " | ANSWER (value): " + value
                + " | ANSWER (index): " + indexValue);
    }

    @Override
    public void onAnswerMultiple(int page, List<String> values, List<Integer> indexValues) {
        Log.d(LOG_TAG, "onAnswerMultiple() | PAGE:  " + page
                + " | ANSWER (values): " + Arrays.toString(values.toArray())
                + " | ANSWER (indexes): " + Arrays.toString(indexValues.toArray()));
    }

    @Override
    public void onAnswerTextBox(int page, String value) {
        Log.d(LOG_TAG, "onAnswerTextBox() | PAGE:  " + page
                + " | ANSWER: " + value);
    }
}
